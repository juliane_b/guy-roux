# Guy Roux Project

This project is a Training tool.

## Prerequisite

- Install Nodemon globally (https://nodemon.io/)
- Clone the repository
- Run the server with nodemon
- Test the /greetings endpoint with postman

## API

### Level 1 - Routing

- Create the post route greetings using a param (ex : /blabla/:id)
- Create a new function greetingSomeone sending back "Hello firstName" (use greetings function as a reference)
- Create a function and a route using a payload (ctx.request.body). the ajax contains a first name and a last name, your function greets this person. (Use koa-body for this purpose)

You can test your code using Postman

### Level 2 - API REST and CRUD

- Recreate the same thing than in the level 1 : router and app.js
- Don't forget to import koa, koa-body and koa-router
- Design a CRUD system where you'll have to get, add, modify and delete object from a virtual object data
- Don't forget to organize your endpoints with REST

## FRONT

### Level 1

Using Vue.js documentation and basic Vue.js concepts, we will modify HelloWorld component to greets the person wisiting our website.

- First, install dependencies inside the greetings folder of the front
- Run the project with npm run serve
- This component will have a variable _msg_ in h1 tag saying "Welcome to my website !" and a h2 tag "Thanks you for your visit, _name_"
- The variable _msg_ can be modified in the parent component/view of HelloWorld.vue (See props in Vue.js documentation)
- The variable _name_ will be created thanks to two inputs in the component : first name, last name (See computed property in Vue.js documentation)

### Level 2

- Run your API (level 2 with CRUD system)
- Install axios
- Create a new js file named as the type of info used in your API (users.js if you deal with users)
- Create two components Form.vue and List.vue
- Form.vue must have two or more inputs and a button "Register". On clicking the button, the informations is sent to your API with a POST request (see Methods in Vue.js documentation)
- List.vue contains a list of the users registered in your "database", you must fill it with a GET request (see templating in Vue.js Documentation : v-if, v-for)

### Level 3

- Using Level 2 of front, imagine a way to make DELETE and PUT requests to delete and modify entries in your "database"
