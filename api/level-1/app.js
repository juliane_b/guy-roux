const Koa = require('koa');

const app = new Koa();

// Use routes
const router = require('./router');
app.use(router.routes());

app.listen(3000);
