/* This file is dedicated to expose endpoints (or routes).
It receives the Ajax calls from the front application */

let Router = require('koa-router');
let router = new Router();

router.get('/greetings', greetings);

async function greetings(ctx, next) {
  ctx.response.status = 200;
  ctx.body = { greetings: 'Hello world !' };

  await next();
}

async function greetingSomeone(firstName) {}

module.exports = router;
